<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Vendor;

class VendorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        $vendors = Vendor::where('user_id', $user_id)->get();
        return view('vendors.index', compact('vendors', $vendors));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'vendor_name'   => 'required|max:50',
        ]);

        $vendor = Vendor::create([
            'user_id'   => $request->user_id,
            'name'      => $request->vendor_name,
        ]);

        $vendor->save();

        return redirect()->back()->with('message', 'Vendor has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vendor = Vendor::findOrFail($id);
        $validator = $request->validate([
            'vendor_name'   => 'required|max:50',
        ]);

        $vendor->update([
            'user_id'   => $request->user_id,
            'name'      => $request->vendor_name,
        ]);

        $vendor->save();

        return redirect()->back()->with('message', 'Vendor has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = Vendor::findOrFail($id);
        $vendor->delete();
        return redirect()->back()->with('message', 'Vendor has been deleted!');
    }
}
