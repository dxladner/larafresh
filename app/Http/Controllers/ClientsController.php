<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Client;

class ClientsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        $clients = Client::where('user_id', $user_id)->get();
        return view('clients.index', compact('clients', $clients));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'company_contact_first_name'    => 'required|max:50',
            'company_contact_last_name'     => 'required|max:255',
            'company_contact_email'         => 'required|max:255',
            'company_contact_phone_number'  => 'required|max:255',
            'company_contact_cell_phone'    => 'required|max:255',
            'company_name'                  => 'required|max:255',
            'company_website'               => 'required|max:255',
            'company_address_one'           => 'required|max:255',
            'company_address_two'           => 'required|max:255',
            'company_city'                  => 'required|max:255',
            'company_state'                 => 'required|max:255',
            'company_zip'                   => 'required|max:255',
            'company_country'               => 'required|max:255',
            'company_business_phone'        => 'required|max:255',
            'company_fax_number'            => 'required|max:255',
            'company_size'                  => 'required|max:255',
            'company_industry'              => 'required|max:255',
            'company_notes'                 => 'required|max:255',

        ]);

        $client = Client::create([
            'user_id'                       => $request->user_id,
            'company_contact_first_name'    => $request->company_contact_first_name,
            'company_contact_last_name'     => $request->company_contact_last_name,
            'company_contact_email'         => $request->company_contact_email,
            'company_contact_phone_number'  => $request->company_contact_phone_number,
            'company_contact_cell_phone'    => $request->company_contact_cell_phone,
            'company_name'                  => $request->company_name,
            'company_website'               => $request->company_website,
            'company_address_one'           => $request->company_address_one,
            'company_address_two'           => $request->company_address_two,
            'company_city'                  => $request->company_city,
            'company_state'                 => $request->company_state,
            'company_zip'                   => $request->company_zip,
            'company_country'               => $request->company_country,
            'company_business_phone'        => $request->company_business_phone,
            'company_fax_number'            => $request->company_fax_number,
            'company_size'                  => $request->company_size,
            'company_industry'              => $request->company_industry,
            'company_notes'                 => $request->company_notes,
        ]);

        $client->save();

        return redirect()->back()->with('message', 'Client has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
