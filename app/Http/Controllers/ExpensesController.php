<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use Auth;
use App\Vendor;

class ExpensesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        $expenses = Expense::where('user_id', $user_id)->get();
        return view('expenses.index', compact('expenses', $expenses));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::id();
        $vendors = Vendor::where('user_id', $user_id)->get();
        return view('expenses.create', compact('vendors', $vendors));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'date'      => 'required|max:50',
            'amount'    => 'required|max:255',
            'vendor'    => 'required|max:255',
            'category'  => 'required|max:255',
            'notes'     => 'nullable',
        ]);

        $expense = Expense::create([
            'user_id'   => $request->user_id,
            'date'      => $request->date,
            'amount'    => $request->amount,
            'vendor'    => $request->vendor,
            'category'  => $request->category,
            'notes'     => $request->notes,
        ]);

        $expense->save();

        return redirect()->back()->with('message', 'Expense has been saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense = Expense::findOrFail($id);
        $user_id = Auth::id();
        $vendors = Vendor::where('user_id', $user_id)->get();
        return view('expenses.edit', compact('expense', $expense, 'vendors', $vendors));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $expense = Expense::findOrFail($id);
        $validator = $request->validate([
            'date'      => 'required|max:50',
            'amount'    => 'required|max:255',
            'vendor'    => 'required|max:255',
            'category'  => 'required|max:255',
            'notes'     => 'nullable',
        ]);

        $expense->update([
            'user_id'   => $request->user_id,
            'date'      => $request->date,
            'amount'    => $request->amount,
            'vendor'    => $request->vendor,
            'category'  => $request->category,
            'notes'     => $request->notes,
        ]);

        $expense->save();

        return redirect()->back()->with('message', 'Expense has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expense = Expense::findOrFail($id);
        $expense->delete();
        return redirect()->back()->with('message', 'Expense has been deleted!');
    }


}
