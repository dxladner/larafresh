<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'company_name', 'company_country', 'company_address_one', 'company_address_two', 'company_city',
        'company_state', 'company_zip', 'company_industry', 'company_size', 'company_business_phone', 'company_fax_number', 
        'company_notes', 'company_send_invoices', 'company_contact_first_name', 'company_contact_last_name', 'company_contact_email', 
        'company_contact_phone_number', 'company_contact_cell_phone',
    ];
}
