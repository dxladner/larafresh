<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('company_name');
            $table->string('company_website');
            $table->string('company_address_one');
            $table->string('company_address_two');
            $table->string('company_city');
            $table->string('company_state');
            $table->string('company_zip');
            $table->string('company_country');
            $table->string('company_business_phone');
            $table->string('company_fax_number');
            $table->string('company_industry');
            $table->string('company_size');
            $table->string('company_notes');
            $table->string('company_send_invoices');
            $table->string('company_contact_first_name');
            $table->string('company_contact_last_name');
            $table->string('company_contact_email');
            $table->string('company_contact_phone_number');
            $table->string('company_contact_cell_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
