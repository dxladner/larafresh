@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Vendors</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif 
                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <h3>Create a Vendor</h3>
                    <form method="post" action="{{ url('vendors') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                        <div class="row">
                            <div class="form-group col-md-3">
                                    <label for="date">Vendor Name:</label>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="vendor_name" name="vendor_name" value="{{ old('vendor_name') }}" />
                            </div>
                            <div class="form-group col-md-3">
                                <button type="submit" class="btn btn-primary">Save Vendor</button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <h3>Vendors</h3>
                    @if(count($vendors))
                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            
                                @foreach($vendors as $vendor)
                                <tr>
                                    <td>{{ $vendor->name }}</td>
                                <td><a href="/vendors/{{ $vendor->id }}/edit" id="editVendor" data-toggle="modal" data-target="#vendorEditModal" class="btn btn-warning bt-sm">Edit</a></td>
                                <td>
                                    <div class="form-group">
                                        <form class="form" role="form" method="POST" action="{{ url('/vendors/' . $vendor->id) }}">
                                            <input type="hidden" name="_method" value="delete">
                                            {{ csrf_field() }}
                                            <input class="btn btn-danger" Onclick="return ConfirmDelete();" type="submit" value="Delete">
                                        </form>
                                    </div>
                                </tr>
                                @endforeach
                            
                        </table>
                    @else 
                        <div class="alert alert-warning">
                            There are no vendors.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Vendor Edit Modal -->
    <div class="modal fade" id="vendorEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Edit a Vendor</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                @if(count($vendors))
                <form class="form" role="form" method="POST" action="{{ url('/vendors/'. $vendor->id) }}">
    
                    {{ method_field('PATCH') }}
                
                    {{ csrf_field() }}
            
                    <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="date">Vendor Name:</label>
                        </div>
                        <div class=form-group col-md-6>
                            <input type="text" class="form-control" id="vendor_name" name="vendor_name" value="{{ $vendor->name }}" />
                        </div>
                    </div>

                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-primary" value="Save Changes" />
            </div>
            </form>
            @endif
          </div>
        </div>
      </div>
@endsection

<script>
$('#vendorEditModal').on('shown.bs.modal', function () {
  $('#editVendor').click('focus')
})
function ConfirmDelete()
{
    var x = confirm("Are you sure you want to delete?");
    return x;
}
</script>
