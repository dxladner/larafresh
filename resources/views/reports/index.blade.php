@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Reports</div>
                <div class="card-body">
                    <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Accounting Reports
                            </button>
                        </h5>
                        </div>
                    
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <a class="dropdown-item" href="#">Balance Sheet</a>
                            <a class="dropdown-item" href="#">Expense Report</a>
                            <a class="dropdown-item" href="#">Payments Collected</a>
                            <a class="dropdown-item" href="#">Profit and Loss</a>
                            <a class="dropdown-item" href="#">Tax Summary</a>
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Clients Report
                            </button>
                        </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            <a class="dropdown-item" href="#">Accounts Aging</a>
                            <a class="dropdown-item" href="#">Revenue by Client</a>
                            <a class="dropdown-item" href="#">Time to Pay</a>
                            <a class="dropdown-item" href="#">Customer Reviews</a>
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Invoice Reports
                            </button>
                        </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            <a class="dropdown-item" href="#">Invoice Details</a>
                            <a class="dropdown-item" href="#">Item Sales</a>
                            <a class="dropdown-item" href="#">Recurring Revenue - Annual</a>
                            <a class="dropdown-item" href="#">Recurring Revenue - Disabled</a>
                            <a class="dropdown-item" href="#">Revenue by Client</a>
                            <a class="dropdown-item" href="#">Revenue by Staff</a>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


