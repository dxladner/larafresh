<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>
        @auth
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Clients
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url('clients/create') }}">Clients</a>
                        <a class="dropdown-item" href="#">Staff</a>
                        <a class="dropdown-item" href="#">Contractors</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Assign Clients</a>
                    </div>
                </li>
                
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Invoices
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Invoices</a>
                        <a class="dropdown-item" href="#">Recurring</a>
                        <a class="dropdown-item" href="#">Received</a>
                        <a class="dropdown-item" href="#">Payments</a>
                        <a class="dropdown-item" href="#">Credits</a>
                        <a class="dropdown-item" href="#">Items</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Estimates
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Estimates</a>
                        <a class="dropdown-item" href="#">Received</a>
                        <a class="dropdown-item" href="#">Items</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Expenses
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url('expenses') }}">Expenses</a>
                        <a class="dropdown-item" href="{{ url('vendors') }}">Vendors</a>
                        <a class="dropdown-item" href="#">Bank Accounts</a>
                        <a class="dropdown-item" href="#">File Import</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Documents
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Documents</a>
                        <a class="dropdown-item" href="#">Browse</a>
                        <a class="dropdown-item" href="#">File Folder Tree</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Reports
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Accounting
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Balance Sheet</a>
                                <a class="dropdown-item" href="#">Expense Report</a>
                                <a class="dropdown-item" href="#">Payments Collected</a>
                                <a class="dropdown-item" href="#">Profit and Loss</a>
                                <a class="dropdown-item" href="#">Tax Summary</a>
                            </div>
                        </li>
                    </div>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Clients Report
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Accounts Aging</a>
                                <a class="dropdown-item" href="#">Revenue by Client</a>
                                <a class="dropdown-item" href="#">Time to Pay</a>
                                <a class="dropdown-item" href="#">Customer Reviews</a>
                            </div>
                        </li>
                    </div>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Invoice Reports
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Invoice Details</a>
                                <a class="dropdown-item" href="#">Item Sales</a>
                                <a class="dropdown-item" href="#">Recurring Revenue - Annual</a>
                                <a class="dropdown-item" href="#">Recurring Revenue - Disabled</a>
                                <a class="dropdown-item" href="#">Revenue by Client</a>
                                <a class="dropdown-item" href="#">Revenue by Staff</a>
                            </div>
                        </li>
                    </div>
                </li>
            </ul>
            @endauth
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @else
                
               
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->first_name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="nav-link" href="#">{{ __('Settings') }}</a>
                                <a class="nav-link" href="#">{{ __('Account') }}</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>