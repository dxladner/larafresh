@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Client</div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <a class="btn btn-primary btn-sm" href="{{ url('clients') }}">Back to Clients</a>
                <div class="card-body">
                   <form method="post" action="{{ url('clients') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                        <h5>Contact</h5>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_contact_first_name">First Name:</label>
                                <input type="text" class="form-control" id="company_contact_first_name" name="company_contact_first_name" value="{{ old('company_contact_first_name') }}">
                            </div>
                        </div>

                        <div class="row">        
                            <div class="form-group col-md-6">
                                <label for="company_contact_last_name">Last Name:</label>
                                <input type="text" class="form-control" id="company_contact_last_name" name="company_contact_last_name" value="{{ old('company_contact_last_name') }}">
                            </div>              
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_contact_email">Email:</label>
                                <input type="email" class="form-control" name="company_contact_email" id="company_contact_email" value="{{ old('company_contact_email') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_contact_phone_number">Work Phone:</label>
                                <input type="text" class="form-control" name="company_contact_phone_number" id="company_contact_phone_number" value="{{ old('company_contact_phone_number') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_contact_cell_phone">Cell Phone:</label>
                                <input type="text" class="form-control" name="company_contact_cell_phone" id="company_contact_cell_phone" value="{{ old('company_contact_cell_phone') }}">
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_name">Company Name:</label>
                                <input type="text" class="form-control" id="company_name" name="company_name" value="{{ old('company_name') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_website">Company Website:</label>
                                <input type="text" class="form-control" id="company_website" name="company_website" value="{{ old('company_website') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_address_one">Company Address One:</label>
                                <input type="text" class="form-control" id="company_address_one" name="company_address_one" value="{{ old('company_address_one') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_address_two">Company Address Two:</label>
                                <input type="text" class="form-control" id="company_address_two" name="company_address_two" value="{{ old('company_address_two') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_city">Company City:</label>
                                <input type="text" class="form-control" id="company_city" name="company_city" value="{{ old('company_city') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_state">Company State:</label>
                                <input type="text" class="form-control" id="company_state" name="company_state" value="{{ old('company_state') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_zip">Company Zip:</label>
                                <input type="text" class="form-control" id="company_zip" name="company_zip" value="{{ old('company_zip') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_country">Company Country:</label>
                                <select class="form-control" name="company_country" id="company_country">
                                @foreach(config('constants.COUNTRY') as $value => $country)
                                    <option value="{{ $value }}">
                                            {{ $country }}
                                    </option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_business_phone">Company Business Phone:</label>
                                <input type="text" class="form-control" id="company_business_phone" name="company_business_phone" value="{{ old('company_business_phone') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_fax_number">Company Fax Number:</label>
                                <input type="text" class="form-control" id="company_fax_number" name="company_fax_number" value="{{ old('company_fax_number') }}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_size">Company Size:</label>
                                <select class="form-control" name="company_size" id="company_size">
                                @foreach(config('constants.COMPANY_SIZE') as $value => $sizes)
                                    <option value="{{ $value }}">
                                            {{ $sizes }}
                                    </option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="company_industry">Industry:</label>
                                <select class="form-control" name="company_industry" id="company_industry">
                                @foreach(config('constants.INDUSTRY') as $value => $industry)
                                    <option value="{{ $value }}">
                                            {{ $industry }}
                                    </option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">            
                            <div class="form-group col-md-6">
                                <label for="company_notes">Notes:</label>
                                <textarea class="form-control" name="company_notes" id="company_notes" >{{ old('company_notes') }}</textarea>
                            </div>                   
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <button type="submit" class="btn btn-success">Save Client</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
