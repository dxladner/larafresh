@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Expenses</div>
                <a class="btn btn-primary btn-sm" href="{{ url('expenses/create') }}">Create a Expense</a>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif 
                    <h3>Expenses</h3>
                    @if(count($expenses))
                        <table class="table table-striped">
                            <tr>
                                <th>Date</th>
                                <th>Category</th>
                                <th>Vendor</th>
                                <th>Notes</th>
                                <th>Amount</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            
                                @foreach($expenses as $expense)
                                <tr>
                                    <td>{{ $expense->date }}</td>
                                    <td>{{ $expense->category }}</td>
                                    <td>{{ $expense->vendor }}</td>
                                    <td>{{ $expense->notes }}</td>
                                    <td>{{ $expense->amount }}</td>
                                <td><a href="/expenses/{{ $expense->id }}/edit" class="btn btn-warning bt-sm">Edit</a></td>
                                <td>
                                    <div class="form-group">
                                        <form class="form" role="form" method="POST" action="{{ url('/expenses/' . $expense->id) }}">
                                            <input type="hidden" name="_method" value="delete">
                                            {{ csrf_field() }}
                                            <input class="btn btn-danger" Onclick="return ConfirmDelete();" type="submit" value="Delete">
                                        </form>
                                    </div>
                                </tr>
                                @endforeach
                            
                        </table>
                    @else 
                        <div class="alert alert-warning">
                            There are no expenses.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

<script>
function ConfirmDelete()
{
    var x = confirm("Are you sure you want to delete?");
    return x;
}
</script>
