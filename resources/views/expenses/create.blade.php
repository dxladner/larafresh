@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Expense</div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <a class="btn btn-primary btn-sm" href="{{ url('expenses') }}">Back to Expenses</a>
                <div class="card-body">
                   <form method="post" action="{{ url('expenses') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="date">Date:</label>
                                <input type="date" class="form-control" name="date" value="{{ old('date') }}">
                            </div>
                        </div>

                        <div class="row">        
                            <div class="form-group col-md-6">
                                <label for="amount">Amount:</label>
                                <input type="text" class="form-control" name="amount" value="{{ old('amount') }}">
                            </div>              
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="vendor">Vendor:</label>
                                <select class="form-control" name="vendor" id="vendor">
                                    @foreach($vendors as $vendor)
                                        <option value="{{ $vendor->name }}">{{ $vendor->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="category">Category:</label>
                                <input type="text" class="form-control" name="category" id="category" value="{{ old('category') }}">
                            </div>
                        </div>

                        <div class="row">            
                            <div class="form-group col-md-6">
                                <label for="notes">Notes:</label>
                                <textarea class="form-control" name="notes" id="notes" >{{ old('notes') }}</textarea>
                            </div>                   
                        </div>

                       

                        <div class="row">
                            <div class="form-group col-md-6">
                                <button type="submit" class="btn btn-success">Save Expense</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
