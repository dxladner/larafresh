@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Expenses</div>
                <a class="btn btn-primary btn-sm" href="{{ url('expenses') }}">Back to Expenses</a>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div class="card-body">
                    <form class="form" role="form" method="POST" action="{{ url('/expenses/'. $expense->id) }}">
        
                        {{ method_field('PATCH') }}
                    
                        {{ csrf_field() }}
                
                        <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="date">Date:</label>
                                <input type="text" class="form-control datepicker" name="date" id="date" value="{{ $expense->date }}">
                            </div>
                        </div>

                        <div class="row">        
                            <div class="form-group col-md-6">
                                <label for="amount">Amount:</label>
                                <input type="text" class="form-control" name="amount" id="amount" value="{{ $expense->amount }}">
                            </div>              
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="vendor">Vendor:</label>
                                <select class="form-control" name="vendor" id="vendor">
                                    <option value="{{ $expense->vendor }}">{{ $expense->vendor}}</option>
                                    @foreach($vendors as $vendor)
                                        <option value="{{ $vendor->name }}">{{ $vendor->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="row">            
                            <div class="form-group col-md-6">
                                <label for="category">Category:</label>
                                <input type="text" class="form-control" name="category" value="{{ $expense->category }}">
                            </div>                   
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="notes">Notes:</label>
                                <textarea class="form-control" name="notes" id="notes" >{{ $expense->notes }}</textarea>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <button type="submit" class="btn btn-success">Save Expense</button>
                            </div>
                        </div>
            
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
    </script>
@endsection
